# What is this?
This repo is home to system.img (ext4), which is used for postmarketOS Hybris implementation.

# Download?
The repo contains ext4_system.img, you'll need to download it using the raw button.

# Installation Guide
Download ext4_system.img, then place it to /var/lib/lxc/android/ under the name:
`system.img`

Reboot the device and postmarketOS should load Halium init, which you could check using dmesg.

